import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import emailjs, { EmailJSResponseStatus } from '@emailjs/browser';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
   form: FormGroup;
   to_name:string="Nilima Pachori";

  constructor(private fb:FormBuilder){
      this.form=fb.group({
        name: new FormControl(),
        email: new FormControl(),
        subject: new FormControl(),
        message: new FormControl(),
      })
  }

      
  sendEmail(form:any){
    // console.log(form.controls.name.value);
    // console.log(form.controls.email.value);
    // console.log(form.controls.subject.value);
    // console.log(form.controls.message.value);

     emailjs.send("service_5738qzq","template_nepzd4s",{
      from_name: form.controls.name.value,
      to_name:  this.to_name,
      email:  form.controls.email.value,
      subject:  form.controls.subject.value,
      message:  form.controls.message.value
      },"nS2Wp_NqMduDuTYku").then(function(res){
        alert("successfully sent message..."+res.status)
        form.reset();
      }).catch(function(err){
        alert("Error occured...! Please check.")
      });

      // this.form.reset();
  }
}
