import { Component } from '@angular/core';
import{NgForm} from "@angular/forms"

@Component({
  selector: 'app-calcarbon',
  templateUrl: './calcarbon.component.html',
  styleUrls: ['./calcarbon.component.css']
})
export class CalcarbonComponent {
  total:number=0;
  calculate(carform:NgForm){
     this.total=carform.value.cfl+carform.value.fan+carform.value.tv+carform.value.wmachine+carform.value.otherA;
  }
}
