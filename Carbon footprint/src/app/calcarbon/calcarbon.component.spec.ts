import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcarbonComponent } from './calcarbon.component';

describe('CalcarbonComponent', () => {
  let component: CalcarbonComponent;
  let fixture: ComponentFixture<CalcarbonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CalcarbonComponent]
    });
    fixture = TestBed.createComponent(CalcarbonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
