import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalcarbonComponent } from './calcarbon/calcarbon.component';
import { WhatIsCCComponent } from './what-is-cc/what-is-cc.component';
import { AboutUsComponent } from './about-us/about-us.component';
import {FormsModule} from "@angular/forms"

@NgModule({
  declarations: [
    AppComponent,
    CalcarbonComponent,
    WhatIsCCComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
