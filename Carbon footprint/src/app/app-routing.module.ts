import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WhatIsCCComponent } from './what-is-cc/what-is-cc.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { CalcarbonComponent } from './calcarbon/calcarbon.component';

const routes: Routes = [
  {path:"Wicc", component:WhatIsCCComponent},
  {path:"About", component:AboutUsComponent},
  {path:"Kycc", component:CalcarbonComponent},
  {path:'', redirectTo:'Kycc', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
