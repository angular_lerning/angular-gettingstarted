import { Component } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router:Router){}
  wiscc(){
    this.router.navigate(["/Wicc"]);
  }
  about(){
    this.router.navigate(["/About"]);
  }
  kyourcc(){
    this.router.navigate(["/Kycc"]);
  }
}
