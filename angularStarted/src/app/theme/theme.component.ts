import { Component, ViewChild,ElementRef,AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css']
})
export class ThemeComponent {
  @ViewChild('name') elname:ElementRef
  @ViewChild('state') state:ElementRef
  ngAfterViewInit(){
    this.elname.nativeElement.style.backgroundColor='black';
    this.elname.nativeElement.style.color='white';
    this.state.nativeElement.style.backgroundColor='Aquamarine';
    this.state.nativeElement.style.color='red';
  }
}
