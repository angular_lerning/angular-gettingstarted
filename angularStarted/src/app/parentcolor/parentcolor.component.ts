import { Component, ViewChild } from '@angular/core';
import { ChangecolorDirective } from '../changecolor.directive';

@Component({
  selector: 'app-parentcolor',
  templateUrl: './parentcolor.component.html',
  styleUrls: ['./parentcolor.component.css']
})
export class ParentcolorComponent {
@ViewChild(ChangecolorDirective) changecolorDirective:ChangecolorDirective

cngcolor(color:string){
  this.changecolorDirective.changeColor(color);
}
}
