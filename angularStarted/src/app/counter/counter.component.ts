import { Component } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent {
   message:string="";
   count:number=0;
   constructor(){}

   increasebyone(){
    this.count=this.count+1;
    this.message="Counter: " +this.count;
   }
   dicreasebyone(){
    this.count=this.count-1;
    this.message="Counter: " +this.count;
   }
}
